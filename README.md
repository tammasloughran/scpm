# SCPM

What does SCPM stand for? I don't know. Whatever you want, I guess. Does it matter? I didn't
really make this for the masses. I made it to teach myself some C and maybe make a tool that can
quickly create a simple project and get it running. That's what it does: creates the directory
tree for a project and manages the build system files.

## Build

```bash
cmake -B build
cmake --build build
sudo cmake --install build
```

### Debug
```bash
cmake -DCMAKE_BUILD_TYPE=Debug -B build-debug
cmake --build build-debug
cmake --install build-debug --prefix /home/tammas/.local --config Debug
```

## Quick start

Initialize a directory as a project:
```bash
$ scpm -i myproject
Creating diretory: myproject
Creating diretory: myproject/src
Creating diretory: myproject/app
Creating new file: myproject/app/main.c
Creating diretory: myproject/doc
Creating new file: myproject/doc/doxygen.conf
Creating new file: myproject/CMakeLists.txt
Creating new file: myproject/src/CMakeLists.txt
Creating new file: myproject/app/CMakeLists.txt
Creating new file: myproject/README.md
```

Project is initialized with placeholders so it can be built immediately.
```bash
$ cd myproject
$ cmake -B build
-- Configuring done
-- Generating done
-- Build files have been written to: /home/tammas/sources/scpm/myproject/build
$ cmake --build build
[ 50%] Building C object app/CMakeFiles/my_project.dir/main.c.o
[100%] Linking C executable my_project
[100%] Built target my_project
```

Add a library to the project.
```bash
$ touch src/foo.c
$ scpm --add-lib=foo src/foo.c
Which executables should this target be added to? (list numbers separated by spaces)
(0) None
(1) my_project
1
$ cmake --build build
[ 25%] Building C object src/CMakeFiles/foo.dir/foo.c.o
[ 50%] Linking C static library libfoo.a
[ 50%] Built target foo
[ 75%] Building C object app/CMakeFiles/my_project.dir/main.c.o
[100%] Linking C executable my_project
[100%] Built target my_project
```

Add a new executable.
```bash 
$ echo "int main() {}" > app/test.c
$ scpm --add-exec=test app/test.c
$ cmake --build build
-- Configuring done
-- Generating done
-- Build files have been written to: /home/tammas/sources/test_program/build
Consolidate compiler generated dependencies of target foo
[ 33%] Built target foo
[ 50%] Building C object app/CMakeFiles/test.dir/test.c.o
[ 66%] Linking C executable test
[ 66%] Built target test
Consolidate compiler generated dependencies of target my_project
[100%] Built target my_project
```

Add a new file to an existing target.
```bash
$ touch src/baa.c
$ scpm --add-files=foo src/baa.c
$ cmake --build build
-- Configuring done
-- Generating done
-- Build files have been written to: /home/tammas/sources/test_program/build
Consolidate compiler generated dependencies of target foo
[ 14%] Building C object src/CMakeFiles/foo.dir/baa.c.o
[ 28%] Linking C static library libfoo.a
[ 42%] Built target foo
Consolidate compiler generated dependencies of target test
[ 71%] Built target test
Consolidate compiler generated dependencies of target my_project
[ 85%] Linking C executable my_project
[100%] Built target my_project
```

