#include <stdio.h>
    // fprintf(), printf(), stderr, stdout
#include <stdlib.h>
    // NULL, EXIT_FAILURE, EXIT_SUCCESS
#include "dropt.h"
    // dropt_bool, dropt_char, dropt_option, dropt_context, dropt_new_context() dropt_parse()
    // dropt_get_error(), dropt_get_error_message(), dropt_error_none, dropt_print_help()
#include "init_project.h"
    // initialize()
#include "cmake_commands.h"
    // add_lib(), add_files()

int main(int argc, char **argv) {
    int count = -1;
    int stop = 0;
    // Destination varbles for options
    dropt_bool show_help = 0;
    dropt_bool show_version = 0;
    dropt_bool verbose = 0;
    dropt_char* init_dir = NULL;
    dropt_char* target_lib = NULL;
    dropt_char* target_of_files = NULL;
    dropt_char* target_exec = NULL;
    // Option table
    dropt_option opt_table[] = {
        //{short, long, description, arg_description, handler, destination, attr, extra_data}
        {'h', "help", "Show help.", NULL, dropt_handle_bool, &show_help, dropt_attr_halt, 0},
        {'\0', "version", "Show version.", NULL, dropt_handle_bool, &show_version,
            dropt_attr_halt, 0},
        {'v', "verbose", "Verbosely show behaviour.", NULL, dropt_handle_bool, &verbose, 0, 0},
        {'i', "init", "Initialize a directory as a project.", "DIR", dropt_handle_string,
                &init_dir, 0, 0},
        {'l', "add-lib", "Add a library to CMake configuration", "LIB-NAME [files, ...]",
                dropt_handle_string, &target_lib, 0, 0},
        {'e', "add-exec", "Add an executable to CMake configuration", "EXEC [files, ...]",
                dropt_handle_string, &target_exec, 0, 0},
        {'f', "add-files", "Add new files to a CMake target", "TARGET [files, ...]",
                dropt_handle_string, &target_of_files, 0, 0},
        {0} // Remaining operands
    };
    dropt_context* opt_context;
    int exit_code = EXIT_SUCCESS;

    // Parse command line.
    opt_context = dropt_new_context(opt_table);

    if (opt_context==NULL) {
        exit_code = EXIT_FAILURE;
    } else {
        char** operands = dropt_parse(opt_context, -1, &argv[1]);
        // Count Operands
        while (stop!=1) {
            ++count;
            if (operands[count]==NULL) {
                stop = 1;
            }
        }
        if (dropt_get_error(opt_context)!=dropt_error_none) {
            fprintf(stderr, "scpm: %s\n", dropt_get_error_message(opt_context));
            exit_code = EXIT_FAILURE;
        } else if (show_help) {
            printf("Usage: scpm [option] [--] [operands]\n\nOptions:\n");
            dropt_print_help(stdout, opt_context, NULL);
        } else if (show_version) {
            printf("scpm 0.1\n");
        } else if (init_dir!=NULL) {
            initialize(init_dir);
        } else if (target_lib!=NULL) {
            add_lib(target_lib, operands, count);
        } else if (target_exec!=NULL) {
            add_exec(target_exec, operands, count);
        } else if (target_of_files!=NULL) {
            add_files(target_of_files, operands, count);
        }
    }

    dropt_free_context(opt_context);
    return exit_code;
}

/*
BSD 2-Clause License

Copyright (c) 2021, Tammas Loughran
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
