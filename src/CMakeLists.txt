add_library(init_project
    init_project.c
    init_project.h
    doxygen_conf.h
)
add_library(file_system_ops
    file_system_ops.c
    file_system_ops.h
    string_len.h
    tinydir/tinydir.h
)
add_library(cmake_commands
    cmake_commands.c
    cmake_commands.h
)

target_include_directories(init_project
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
)
target_include_directories(file_system_ops
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/tinydir
)
target_include_directories(cmake_commands
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
)

add_subdirectory(dropt)
# Disable warnings from the dropt external library.
set_target_properties(dropt PROPERTIES COMPILE_FLAGS "-w")
# Disable compilation of dropt executables and droptxx library.
set_target_properties(
    dropt_example
    test_dropt
    droptxx
    PROPERTIES EXCLUDE_FROM_ALL 1
    EXCLUDE_FROM_DEFAULT_BUILD 1
)

/*
BSD 2-Clause License

Copyright (c) 2021, Tammas Loughran
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
