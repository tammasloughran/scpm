/**
 * cmake_commands.c contains functions that alter CMakeLists.txt files.
 */
#include <stdlib.h>
    // NULL, EXIT_FAILURE, malloc(), calloc(), free()
#include <stdio.h>
    // FILE, stdin, printf(), scanf(), fscanf(), fprintf(), fclose(), fopen(), rewind()
#include <string.h>
    // strcpy(), strcmp(), strncmp(), strcat(), strlen(), strtok(), strtol()
#include <sys/stat.h>
    // stat()
#include "string_len.h"
    // DIR_LEN, FILE_LEN, FULL_LEN
#include "tinydir/tinydir.h"
    // tinydir_dir, tinydir_file, tinydir_open(), tinydir_read_file(), tinydir_next()
    // tinydir_close()
#include "file_system_ops.h"
    // SourceFile, base_dir(), scan_files(), scan_folders(), make_file()
// Interface import
#include "cmake_commands.h"

/*
 * Lines is a char array, an array of which will hold the file buffer.
 */
typedef char Lines[256];


/*
 * count_lines counts number of lines in a file.
 */
static int count_lines(FILE* in_file) {
    char buffer[FULL_LEN];
    int nlines = 0;
    while (fgets(buffer, FULL_LEN, in_file)!=NULL) {
        ++nlines;
    }
    rewind(in_file);
    return nlines;
}


/*
 * find_app searches parent directories for a directory named app and returns the full name
 *      of the CMakeLists.txt.
 *
 * Arguments:
 *      char* current_dir - string containing current directory to start search.
 * Returns:
 *      char* app_cmake_file_name - string containing CMakeLists.txt file.
 */
static char* find_app(char* current_dir) {
    SourceFolder* local_folders;
    int exists = 0;
    int i;
    int ndirs;
    int num_itter = 0;
    int stop = 0;
    char* app_cmake_file_name = NULL;
    char temp_dir[DIR_LEN];

    while (exists==0) {
        local_folders = scan_folders(current_dir, &ndirs);
        for (i=0; i<ndirs+1; ++i) {
            if (strcmp(local_folders[i].folder_name, "app")==0) {
                exists = 1;
                app_cmake_file_name = malloc(FULL_LEN);
                if (app_cmake_file_name==NULL) {
                    printf("ERROR in function find_app(): Failed to allocate memory.");
                    exit(EXIT_FAILURE);
                }
                strcpy(app_cmake_file_name, current_dir);
                strcat(app_cmake_file_name, "app/CMakeLists.txt");
            }
        }
        strcpy(temp_dir, current_dir);
        strcpy(current_dir, "../");
        strcat(temp_dir, current_dir);
        strcpy(current_dir, temp_dir);
        ++num_itter;
        if (num_itter>5) {
            printf("ERROR in function find_app(): Could not find app folder in parent"
                   " directories.");
            exit(EXIT_FAILURE);
        }
    }
    return app_cmake_file_name;
}


/*
 * file_name_only strips the path from a filename.
 *
 * Arguments:
 *      char* full_file - string containing full path and filename.
 * Returns:
 *      char* file_name - string containing full file name only.
 */
static char* file_name_only(char* full_file) {
    int n_chars = 0;
    char* current_char;
    char* file_name;
    current_char = full_file;
    while (*current_char!='\0') {
        ++n_chars;
        ++current_char;
    }
    while (n_chars>0) {
        if (*current_char=='/') {
            file_name = current_char+1;
            break;
        }
        --n_chars;
        --current_char;
        file_name = current_char;
    }
    return file_name;
}


/**
 * Add a library to the src directory.
 *
 * Arguments:
 *      char* target - String for target library
 *      char** files - Strings of file names to add to library
 */
void add_lib(char* target, char** files, int count) {
    int i = 0;
    int j = 0;
    int exists = 0;
    int n_execs = 0;
    int n_files = 0;
    int nlines = 0;
    int stop = 0;
    int same_dir = 0;
    int* n;
    char* base_dir_first;
    char cmake_file_name[FULL_LEN];
    char response[FULL_LEN];
    char* app_cmake_file_name;
    char* junk;
    char* token;
    char buffer[FULL_LEN];
    char* line_buffer;
    size_t line_size = 0;
    char executables[50][128] = {'\0'};
    FILE* cmake_file;
    SourceFile* files_in_base;
    Lines* file_buffer;

    // Locate CMakeLists.txt. If one exists in the same directory as the first file, use that.
    base_dir_first = base_dir(files[0]);
    n = calloc(1, sizeof (int));
    files_in_base = scan_files(base_dir_first, n);
    if (files_in_base!=NULL) {
        strcpy(cmake_file_name, base_dir_first);
        for (i=0; i<*n; ++i) {
            if (strcmp(files_in_base[i].file_name, "CMakeLists.txt")==0) {
                strcat(cmake_file_name, files_in_base[i].file_name);
                exists = 1;
                cmake_file = fopen(cmake_file_name, "a");
                same_dir = 1;
                break;
            }
        }
        if (exists==0) {
            // If CMakeLists.txt does not exist then make it.
            cmake_file = make_file(base_dir_first, "CMakeLists.txt");
            fclose(cmake_file);
            strcat(cmake_file_name, "CMakeLists.txt");
            cmake_file = fopen(cmake_file_name, "a");
        }
    } else {
        printf("ERROR: file not found. %s", base_dir_first);
        exit(EXIT_FAILURE);
    }
    free(files_in_base);

    // Check if already exists.
    while (fscanf(cmake_file, "%s", buffer)==1) {
        char add_lib_line[FULL_LEN] = "add_library(";
        strcat(add_lib_line, target);
        strcat(add_lib_line, "\n");
        if (strcmp(buffer, add_lib_line)==0) {
            printf("ERROR: library already exists in CMakeLists.txt file.");
            exit(EXIT_FAILURE);
        }
    }

    // Add library to CMakeLists.txt
    fprintf(cmake_file, "add_library(%s\n", target);
    // Count number of files in files list.
    stop = 0;
    while (stop==0) {
        if (files[n_files]!=NULL) {
            ++n_files;
        } else {
            stop = 1;
        }
    }
    for (i=0; i<n_files; ++i) {
        if (same_dir==1) {
            fprintf(cmake_file, "    %s\n", file_name_only(files[i]));
        } else {
            fprintf(cmake_file, "    %s\n", files[i]);
        }
    }
    fprintf(cmake_file, ")\n");
    fclose(cmake_file);

    // Locate apps that use the target library
    app_cmake_file_name = find_app(base_dir_first);
    free(base_dir_first);
    if (app_cmake_file_name==NULL) return;

    // Create list of executables
    cmake_file = fopen(app_cmake_file_name, "r");
    while (fscanf(cmake_file, "%s", buffer)==1) {
        if (strncmp(buffer, "add_executable(", 15)==0) {
            strcpy(executables[n_execs], &buffer[15]);
            ++n_execs;
        }
    }
    if (n_execs==0) {
        printf("No executable targets found in %s\n", app_cmake_file_name);
    }
    rewind(cmake_file);

    // Display prompt:
    printf("Which executables should this target be added to? (list numbers separated by "
            "spaces)\n");
    printf("(0) None\n");
    for (i=1; i<n_execs+1; ++i) {
        printf("(%i) %s\n", i, executables[i-1]);
    }
    fgets(response, FULL_LEN, stdin);
    if (strcmp(response, "0\n")==0) return;
    if (strcmp(response, "\n")==0) return;

    // Count lines and read into file buffer.
    nlines = count_lines(cmake_file);
    file_buffer = calloc(nlines, sizeof (Lines));
    if (file_buffer==NULL) {
        printf("ERROR in function add_lib(): Failed to allocate memory.");
        exit(EXIT_FAILURE);
    }
    for (i=0; i<nlines; ++i) {
        getline(&line_buffer, &line_size, cmake_file);
        strcpy(file_buffer[i], line_buffer);
    }
    fclose(cmake_file);

    // Loop over all responses
    token = malloc(100);
    junk = malloc(100);
    token = strtok(response, " ");
    while (token!=NULL) {
        j = strtol(token, &junk, 10);
        cmake_file = fopen(app_cmake_file_name, "w");
        strcpy(cmake_file_name, "target_link_libraries(");
        strcat(cmake_file_name, executables[j-1]);
        strcat(cmake_file_name, "\n");
        for (i=0; i<nlines; ++i) {
            fprintf(cmake_file, "%s", file_buffer[i]);
            // Add entries to app CMake file target.
            if (strcmp(cmake_file_name, file_buffer[i])==0) {
                fprintf(cmake_file, "    LINK_PUBLIC %s\n", target);
            }
        }
        // Flush file and reallocate file buffer.
        fclose(cmake_file);
        free(file_buffer);
        cmake_file = fopen(app_cmake_file_name, "r");
        nlines = count_lines(cmake_file);
        file_buffer = calloc(nlines, sizeof (Lines));
        if (file_buffer==NULL) {
            printf("ERROR in function add_lib(): Failed to allocate memory.");
            exit(EXIT_FAILURE);
        }
        for (i=0; i<nlines; ++i) {
            fscanf(cmake_file, "%s", buffer);
            strcpy(file_buffer[i], buffer);
        }
        fclose(cmake_file);

        // Increment response token
        token = strtok(NULL, " ");
    }

    // Free the file buffer.
    free(file_buffer);
    free(app_cmake_file_name);
    free(token);
}


/**
 * Add an executable to the app directory.
 *
 * Arguments:
 *      char* target - String of target executable.
 *      char** files - Strings of file names to add to executable.
 */
void add_exec(char* target, char** files, int count) {
    int exists = 0;
    int i = 0;
    int same_dir = 0;
    int* nfiles;
    int stop = 0;
    char* base_dir_first;
    char* cmake_file_name;
    FILE* cmake_file;
    SourceFile* files_in_base;

    // Locate CMakeLists.txt. If one exists in the same directory as the first file, use that.
    base_dir_first = base_dir(files[0]);
    nfiles = malloc(sizeof (int));
    files_in_base = scan_files(base_dir_first, nfiles);
    if (files_in_base!=NULL) {
        cmake_file_name = malloc(FILE_LEN);
        strcpy(cmake_file_name, base_dir_first);
        for (i=0; i<*nfiles; ++i) {
            if (strcmp(files_in_base[i].file_name, "CMakeLists.txt")==0) {
                strcat(cmake_file_name, "CMakeLists.txt");
                exists = 1;
                cmake_file = fopen(cmake_file_name, "a");
                same_dir = 1;
                break;
            }
        }
        if (exists==0) {
            // If CMakeLists.txt does not exist then make it.
            cmake_file = make_file(base_dir_first, "CMakeLists.txt");
            fclose(cmake_file);
            strcat(cmake_file_name, "CMakeLists.txt");
            cmake_file = fopen(cmake_file_name, "a");
        }
    } else {
        printf("ERROR: file not found. %s", base_dir_first);
        exit(EXIT_FAILURE);
    }
    free(base_dir_first);
    free(files_in_base);

    // Add executable section.
    fprintf(cmake_file, "add_executable(%s\n", target);
    for (i=0; i<count; ++i) {
        if (same_dir==1) {
            fprintf(cmake_file, "    %s\n", file_name_only(files[i]));
        } else {
            fprintf(cmake_file, "    %s\n", files[i]);
        }
    }
    fprintf(cmake_file, ")\n");
    fclose(cmake_file);
}


/**
 * Add files to a target.
 *
 * Arguments:
 *      char* target - String of target.
 *      char** files - Strings of file names to add to target.
 *      int count - Number of files in files array.
 */
void add_files(char* target, char** files, int count) {
    int exists = 0;
    int i;
    int j;
    int* n;
    int nlines = 0;
    char buffer[FULL_LEN];
    char* base_dir_first;
    char* cmake_file_name;
    char* match;
    FILE* cmake_file;
    SourceFile* files_in_base;
    Lines* file_buffer;

    // Locate CMakeLists.txt. If one exists in the same directory as the first file, use that.
    base_dir_first = base_dir(files[0]);
    n = malloc(sizeof (int));
    files_in_base = scan_files(base_dir_first, n);
    if (files_in_base!=NULL) {
        cmake_file_name = malloc(FILE_LEN);
        strcpy(cmake_file_name, base_dir_first);
        for (i=0; i<*n; ++i) {
            if (strcmp(files_in_base[i].file_name, "CMakeLists.txt")==0) {
                strcat(cmake_file_name, files_in_base[i].file_name);
                exists = 1;
                cmake_file = fopen(cmake_file_name, "r");
                break;
            }
        }
        if (exists==0) {
            // If CMakeLists.txt does not exist then make it.
            cmake_file = make_file(base_dir_first, "CMakeLists.txt");
            fclose(cmake_file);
            strcat(cmake_file_name, "CMakeLists.txt");
            cmake_file = fopen(cmake_file_name, "r");
        }
    } else {
        printf("ERROR: file not found. %s", base_dir_first);
        exit(EXIT_FAILURE);
    }

    // Count lines and read into file buffer.
    nlines = count_lines(cmake_file);
    file_buffer = calloc(nlines, sizeof (Lines));
    if (file_buffer==NULL) {
        printf("ERROR in function add_lib(): Failed to allocate memory.");
        exit(EXIT_FAILURE);
    }
    for (i=0; i<nlines; ++i) {
        fscanf(cmake_file, "%s", buffer);
        strcpy(file_buffer[i], buffer);
    }
    fclose(cmake_file);

    // Insert files at target.
    cmake_file = fopen(cmake_file_name, "w");
    match = malloc(FILE_LEN);
    strcpy(match, "add_library(");
    strcat(match, target);
    for (i=0; i<nlines; ++i) {
        fprintf(cmake_file, "%s\n", file_buffer[i]);
        // Add entries to app CMake file.
        if (strstr(file_buffer[i], match)!=NULL) {
            for (j=0; j<count; ++j) {
                fprintf(cmake_file, "    %s\n", file_name_only(files[j]));
            }
        }
    }
    fclose(cmake_file);
    free(file_buffer);
    free(base_dir_first);
    free(files_in_base);
}

/*
BSD 2-Clause License

Copyright (c) 2021, Tammas Loughran
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
