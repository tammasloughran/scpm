// Include guard
#ifndef CMAKE_COMMANDS_H
#define CMAKE_COMMANDS_H
// Namespace mangling
#define add_lib cmake_commands_add_lib
#define add_exec cmake_commands_add_exec
#define add_files cmake_commands_add_files

/**
 * Add a library to the src directory.
 *
 * Arguments:
 *      char* target - String for target library
 *      char** files - Strings of file names to add to library
 *      int count    - Number of files in operands
 */
extern void add_lib(char* target, char** files, int count);

/**
 * Add an executable to the app directory.
 *
 * Arguments:
 *      char* target - String of target executable.
 *      char** files - Strings of file names to add to executable.
 *      int count    - Number of files in operands
 */
extern void add_exec(char* target, char** files, int count);

/**
 * Add files to a target.
 *
 * Arguments:
 *      char* target - String of target.
 *      char** files - Strings of file names to add to target.
 *      int count    - Number of files in operands
 */
extern void add_files(char* target, char** files, int count);

#endif

/*
BSD 2-Clause License

Copyright (c) 2021, Tammas Loughran
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
