/**
 * file_system_ops.c performs operation on the file system.
 */

#include <stdlib.h>
    // malloc()
#include <stdio.h>
    // FILE, printf()
#include <string.h>
    // strcpy(), strcat()
#include <sys/stat.h>
    // stat()
#include "string_len.h"
    // DIR_LEN, FILE_LEN
#include "tinydir/tinydir.h"
    // tinydir_dir, tinydir_file, tinydir_open(), tinydir_read_file(), tinydir_next()
    // tinydir_close()
// Interface import
#include "file_system_ops.h"


/**
 * Extract base directory.
 */
char* base_dir(char* string) {
    int i;
    int j;
    char* result;

    // Find end of base direcory
    for (i=strlen(string)-1; i<strlen(string); --i) {
        if (string[i]=='/') {
            j = i;
            break;
        } else if (i==0) {
            return "./";
        }
    }
    // Transfer to result until the end of the base string.
    result = malloc(j+1);
    if (result==NULL) {
        printf("ERROR in function base_dir(): Failed to allocate memory.");
        exit(EXIT_FAILURE);
    }
    for (i=0; i<=j; ++i) {
        strcpy(&result[i], &string[i]);
    }
    strcpy(&result[j+1], "\0");
    return result;
}


/**
 * Create a new directory.
 */
void make_dir(char* path, char* folder) {
    char new_dir[DIR_LEN];
    struct stat st = {0};

    // Concatenate path and directory name.
    strcpy(new_dir, path);
    strcat(new_dir, folder);
    // Make the new directory if it does not exist.
    if (stat(new_dir, &st)==-1) {
        printf("Creating diretory: %s\n", new_dir);
        mkdir(new_dir, 0775);
    }
}


/**
 * Create a new file.
 */
FILE* make_file(char* path, char* file_name) {
    char new_file_name[FILE_LEN];
    struct stat st = {0};
    FILE* new_file = NULL;

    // Concatenate path and file name.
    strcpy(new_file_name, path);
    strcat(new_file_name, file_name);
    if (stat(new_file_name, &st)==-1) {
        printf("Creating new file: %s\n", new_file_name);
        new_file = fopen(new_file_name, "w");
    }
    return new_file;
}


/**
 * scan_files searches a directory for files and returns an array of SourceFile.
 */
SourceFile* scan_files(char* directory, int* n_files) {
    int nfiles = 0;
    int i = 0;
    tinydir_dir dir;
    tinydir_file file;
    SourceFile* file_list;

    // Count number of regular files.
    tinydir_open(&dir, directory);
    while (dir.has_next) {
        tinydir_readfile(&dir, &file);
        if (file.is_reg) {
            ++nfiles;
        }
        tinydir_next(&dir);
    }
    tinydir_close(&dir);

    // If there are no files return NULL
    if (nfiles==0) return NULL;

    // Create array of files.
    file_list = calloc(nfiles, sizeof(SourceFile));
    tinydir_open(&dir, directory);
    while (dir.has_next) {
        tinydir_readfile(&dir, &file);
        if (file.is_reg) {
            strcpy(file_list[i].path, directory);
            strcpy(file_list[i].file_name, file.name);
            strcpy(file_list[i].full_name, file.path);
            ++i;
        }
        tinydir_next(&dir);
    }
    tinydir_close(&dir);
    *n_files = nfiles;
    return file_list;
}


/**
 * scan_folder searches a directory for folders and returns an array of SourceFolder.
 */
SourceFolder* scan_folders(char* directory, int* ndirs_out) {
    int ndirs = 0;
    int i = 0;
    tinydir_dir dir;
    tinydir_file file;
    SourceFolder* folder_list;

    // Count the number of directories.
    tinydir_open(&dir, directory);
    while (dir.has_next) {
        tinydir_readfile(&dir, &file);
        if (file.is_dir) {
            ++ndirs;
        }
        tinydir_next(&dir);
    }
    tinydir_close(&dir);

    // If there are no folders return NULL
    if (ndirs==0) return NULL;

    // Create array of folders.
    folder_list = calloc(ndirs, sizeof(SourceFolder));
    tinydir_open(&dir, directory);
    while (dir.has_next) {
        tinydir_readfile(&dir, &file);
        if (file.is_dir) {
            strcpy(folder_list[i].folder_name, file.name);
            strcpy(folder_list[i].full_name, file.path);
            ++i;
        }
        tinydir_next(&dir);
    }
    tinydir_close(&dir);
    *ndirs_out = ndirs;
    return folder_list;
}

/*
BSD 2-Clause License

Copyright (c) 2021, Tammas Loughran
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
