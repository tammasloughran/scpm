// Include guard
#ifndef FILE_SYSTEM_OPS_H
#define FILE_SYSTEM_OPS_H
// Namespace mangling
#define base_dir file_system_ops_base_dir
#define make_dir file_system_ops_make_dir
#define make_file file_system_ops_make_file

#include "string_len.h"

/**
 * Extract base directory.
 */
extern char* base_dir(char* string);

/**
 * @brief make_dir makes a new directory from path and folder.
 */
extern void make_dir(char* path, char* folder);

/**
 * make_file makes a new empty file from path and file_name
 */
extern FILE* make_file(char* path, char* file_name);

/**
 * SourceFile contains information about the base directory, name and full name of a source file.
 */
typedef struct {
    char path[DIR_LEN];
    char file_name[FILE_LEN];
    char full_name[FULL_LEN];
} SourceFile;

/**
 * SourceFolder contains information about the base directory, name and full name of a source
 * folder.
 */
typedef struct {
    char path[DIR_LEN];
    char folder_name[FILE_LEN];
    char full_name[FULL_LEN];
} SourceFolder;

/**
 * scan_files searches a directory for files and returns an array of SourceFile.
 * directory - string containing name of directory to scan.
 * n_files - int pointer returning number of files in directory.
 */
extern SourceFile* scan_files(char* directory, int* n_files);

/**
 * scan_folder searched a directory for folders and returns an array of SourceFolder.
 */
extern SourceFolder* scan_folders(char* directory, int* ndirs_out);

#endif

/*
BSD 2-Clause License

Copyright (c) 2021, Tammas Loughran
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
