/**
 * init_project.c initializes a directory as a source code project.
 */
#include <stdlib.h>
    // system()
#include <stdio.h>
    // NULL, FILE, fprintf(), fclose()
#include <string.h>
    // strcpy(), strcat()
#include <sys/stat.h>
    // stat()
#include "file_system_ops.h"
    // make_dir(), make_file()
#include "doxygen_conf.h"
    // doxygen_conf1, doxygen_conf2, doxygen_conf3, doxygen_conf4
// interface import
#include "init_project.h"

/**
 * Trim leading whitespace
 */
static char* ltrim(char* string, char junk) {
    char* original = string;
    char* p = original;
    int trimmed = 0;
    do {
        if (*original!=junk || trimmed) {
            trimmed = 1;
            *p++ = *original;
        }
    }
    while (*original++ != '\0');
    return string;
}

/**
 * Initialize directory as a project.
 */
void initialize(char* directory) {
    FILE* doxygen_conf_file;
    FILE* readme_file;
    FILE* cmake_file;
    FILE* main_file;
    char project_title[512];
    char git_init_command[512];
    char git_add_command[512];
    char string_char;
    int i, j, blank_it;

    // Create project directory.
    make_dir("\0", directory);

    // Create src directory.
    make_dir(directory, "/src");

    // Create app directory.
    make_dir(directory, "/app");

    // Create placeholder main.c
    main_file = make_file(directory, "/app/main.c");
    fprintf(main_file, "\nint main(int argc, char **argv) {\n\n}\n");
    fclose(main_file);

    // Create a doc directory.
    make_dir(directory, "/doc");

    // Create doc/doxygen.doc
    doxygen_conf_file = make_file(directory, "/doc/doxygen.conf");
    if (doxygen_conf_file!=NULL) {
        for (i=0; i<100; i++) {
            fprintf(doxygen_conf_file, "%s", doxygen_conf1[i]);
        }
        for (i=0; i<100; i++) {
            fprintf(doxygen_conf_file, "%s", doxygen_conf2[i]);
        }
        for (i=0; i<100; i++) {
            fprintf(doxygen_conf_file, "%s", doxygen_conf3[i]);
        }
        for (i=0; i<32; i++) {
            fprintf(doxygen_conf_file, "%s", doxygen_conf4[i]);
        }
        fclose(doxygen_conf_file);
    }

    // Create cmake files.
    cmake_file = make_file(directory, "/CMakeLists.txt");
    if (cmake_file!=NULL) {
        // Trim until last / character to get project folder to use as project title.
        strcpy(project_title, directory);
        blank_it = 0;
        for (i=strlen(directory)-1; i>=0; i--) {
            string_char = directory[i];
            if ((string_char=='/') || blank_it) {
                strcpy(&project_title[i], " ");
                blank_it = 1;
            }
        }
        ltrim(project_title, ' ');

        // Insert required cmake commands
        fprintf(cmake_file, "cmake_minimum_required(VERSION 3.0)\n");
        fprintf(cmake_file, "project(%s\n", project_title);
        fprintf(cmake_file, "    VERSION 1.0\n");
        fprintf(cmake_file, "    LANGUAGES C\n");
        fprintf(cmake_file, ")\n");
        fprintf(cmake_file, "add_subdirectory(src)\n");
        fprintf(cmake_file, "add_subdirectory(app)\n");
    }
    fclose(cmake_file);
    cmake_file = make_file(directory, "/src/CMakeLists.txt");
    if (cmake_file!=NULL) fclose(cmake_file);
    cmake_file = make_file(directory, "/app/CMakeLists.txt");
    if (cmake_file!=NULL) {
        fprintf(cmake_file, "add_executable(my_project\n    main.c\n)\n\n");
        fprintf(cmake_file, "target_link_libraries(my_project\n)\n");
        fclose(cmake_file);
    }

    // Create a README.md if it does not exist
    readme_file = make_file(directory, "/README.md");

    // Initialize git
    strcpy(git_init_command, "git init -q ");
    strcat(git_init_command, directory);
    strcat(git_init_command, "\n");
    system(git_init_command);
}

/*
BSD 2-Clause License

Copyright (c) 2021, Tammas Loughran
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
